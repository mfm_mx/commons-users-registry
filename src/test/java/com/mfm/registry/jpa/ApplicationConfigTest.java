package com.mfm.registry.jpa;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mfm.registry.jpa.ApplicationConfig;
import com.mfm.registry.jpa.core.OnlineBankingUserRepository;


/**
 * Test case bootstrapping JavaConfig to validate configuration.
 * 
 */
public class ApplicationConfigTest {

	@Test
	public void bootstrapAppFromJavaConfig() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();

		System.out.println("THE CONTEXT: "+context);
		OnlineBankingUserRepository repository = (OnlineBankingUserRepository)
				context.getBean(OnlineBankingUserRepository.class);
		System.out.println("THE BEAN: "+repository);
		context.close();
	}
}
