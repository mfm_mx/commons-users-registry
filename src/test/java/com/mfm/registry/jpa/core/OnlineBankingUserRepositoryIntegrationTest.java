package com.mfm.registry.jpa.core;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mfm.registry.jpa.ApplicationConfig;

public class OnlineBankingUserRepositoryIntegrationTest {

	@Autowired
	//OnlineBankingUserRepository repository;

	@Test
	public void savesCustomerCorrectly() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();

		System.out.println("THE CONTEXT: "+context);

		OnlineBankingUserRepository repository = (OnlineBankingUserRepository)
				context.getBean(OnlineBankingUserRepository.class);

		OnlineBankingUserLoginAttemptsRepository repositoryLoginAttempts = (OnlineBankingUserLoginAttemptsRepository)
				context.getBean(OnlineBankingUserLoginAttemptsRepository.class);

		OnlineBankingUserSmartDeviceRepository repositorySmartDevice = (OnlineBankingUserSmartDeviceRepository)
				context.getBean(OnlineBankingUserSmartDeviceRepository.class);

		int max=5;
		//this.insertRegistries(repository, max);
		//this.deleteRegistries(repository, max);
		this.insertRegistries(repository, repositoryLoginAttempts, repositorySmartDevice, max);
		for (int j=0; j<max; j++){
			this.updateRegistries(repository, repositoryLoginAttempts, repositorySmartDevice, max, j*max);
		}
		this.findSpecificClients(repository, repositoryLoginAttempts, repositorySmartDevice, max);
		this.findSpecificAliases(repository, repositoryLoginAttempts, repositorySmartDevice, max);
		context.close();
	}

	private void insertRegistries(
			OnlineBankingUserRepository repository, 
			OnlineBankingUserLoginAttemptsRepository repositoryLoginAttempts,
			OnlineBankingUserSmartDeviceRepository repositorySmartDevice,
			int max){

		java.util.Date date = new java.util.Date();
		java.sql.Timestamp d = new java.sql.Timestamp( date.getTime() );

		for (int i=0; i<max; i++){
			OnlineBankingUser U = new OnlineBankingUser(
				"000100111"+i,
				"user_alias"+i,
				"A",
				d,
				d,
				"the fullname",
				"Name",
				"",
				"First Surname",
				"Second Surname",
				"106000544584",
				"USERRFC10002000",
				"user_email@mail.com",
				"DK",
				"SALT",
				"IV" + i,
				"IV_SALT" + i,
				"LHASH");

			OnlineBankingUser result = repository.save(U);
			System.out.println("INSERTED: "+result.getId());

			OnlineBankingUserLoginAttempts L = new OnlineBankingUserLoginAttempts(
					"000100111"+i,
					0,
					d,
					d);
			
			OnlineBankingUserLoginAttempts resultLa = 
					repositoryLoginAttempts.save(L);
			
			System.out.println("INSERTED LA: "+resultLa.getId());

			OnlineBankingUserSmartDevice sd = new OnlineBankingUserSmartDevice(
					"000100111"+i,
					"manufacturer"+i,
					"brand"+i,
					"model"+i,
					"os"+i,
					"osVersion"+i,
					"tokenName"+i,
					0,
					d,
					d);
			
			OnlineBankingUserSmartDevice resultSd = 
					repositorySmartDevice.save(sd);
			
			System.out.println("INSERTED SD: "+resultSd.getId());
		}
	}

	/*
	private void deleteRegistries(
			OnlineBankingUserRepository repository, 
			OnlineBankingUserLoginAttemptsRepository repositoryLoginAttempts,
			int max){

		for (int i=0; i<max; i++)
		{
			OnlineBankingUser U = new OnlineBankingUser(
				"000100111"+i);

			repository.delete(U);
			System.out.println("DELETED: "+("000100111"+i));
		}
	}
	*/

	private void updateRegistries(
			OnlineBankingUserRepository repository, 
			OnlineBankingUserLoginAttemptsRepository repositoryLoginAttempts, 
			OnlineBankingUserSmartDeviceRepository repositorySmartDevice, 
			int max, 
			int offset){

		java.util.Date date = new java.util.Date();
		java.sql.Timestamp d = new java.sql.Timestamp( date.getTime() );

		for (int i=0; i<max; i++) {

			OnlineBankingUser U = repository.findByClientNumber("000100111"+i);

			if (U != null){

				U.setClientAlias("NEW_ALIAS"+(i+offset));
				repository.save(U);
				System.out.println("UPDATED : "+U.getClientNumber()+"; "+U.getClientAlias());

				OnlineBankingUserLoginAttempts L = new OnlineBankingUserLoginAttempts(
						"000100111"+i,
						max+offset,
						d,
						d);

				OnlineBankingUserLoginAttempts resultLa = 
						repositoryLoginAttempts.save(L);

				System.out.println("UPDATED LA: "+resultLa.getId());

				OnlineBankingUserSmartDevice sd = new OnlineBankingUserSmartDevice(
						"000100111"+i,
						"manufacturer"+max+offset,
						"brand"+max+offset,
						"model"+max+offset,
						"os"+max+offset,
						"osVersion"+max+offset,
						"tokenName"+max+offset,
						0,
						d,
						d);
				
				OnlineBankingUserSmartDevice resultSd = 
						repositorySmartDevice.save(sd);
				
				System.out.println("UPDATED SD: "+resultSd.getId());
			}
		}
	}

	private void findSpecificClients(
			OnlineBankingUserRepository repository, 
			OnlineBankingUserLoginAttemptsRepository repositoryLoginAttempts, 
			OnlineBankingUserSmartDeviceRepository repositorySmartDevice, 
			int max){

		int offset = max;
		for (int i=0; i<max+offset; i++) {

			OnlineBankingUser U = repository.findByClientNumber("000100111"+i);
			OnlineBankingUserLoginAttempts L = repositoryLoginAttempts.findByClientNumber("000100111"+i);

			if (U != null){
				System.out.println("FOUND CLIENT: "+U.getClientNumber()+"; "+U.getClientAlias());
			} else {
				System.out.println("CLIENT NOT FOUND: "+"000100111"+i);
			}

			if (L != null){
				System.out.println("FOUND CLIENT LA: "+L.getClientNumber()+"; "+L.getLoginAttemps());
			} else {
				System.out.println("CLIENT LA NOT FOUND: "+"000100111"+i);
			}
		}
	}

	private void findSpecificAliases(
			OnlineBankingUserRepository repository, 
			OnlineBankingUserLoginAttemptsRepository repositoryLoginAttempts, 
			OnlineBankingUserSmartDeviceRepository repositorySmartDevice, 
			int max){

		int offset = max;
		for (int i=0; i<max+offset; i++) {

			OnlineBankingUser U = repository.findByClientAlias("NEW_ALIAS"+i);
			if (U != null){
				System.out.println("FOUND ALIAS: "+U.getClientNumber()+"; "+U.getClientAlias());
			} else {
				System.out.println("ALIAS NOT FOUND: "+"NEW_ALIAS"+i);
			}
		}
	}
}