package com.mfm.registry.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Centralizes all the necessary imports of all the configurations needed
 * (Only one in this case)
 * 
 * @author Alejandro Aguilera Vega
 *
 */
@Configuration
@EnableJpaRepositories
@Import(InfrastructureConfig.class)
public class ApplicationConfig {

}
