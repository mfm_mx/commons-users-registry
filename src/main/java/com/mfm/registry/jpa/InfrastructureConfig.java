package com.mfm.registry.jpa;

//import javax.naming.Context;
//import javax.naming.InitialContext;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * /**
 * Common infrastructure configuration class to setup a Spring container and infrastructure components like a
 * {@link DataSource}, a {@link EntityManagerFactory} and a {@link PlatformTransactionManager}. 
 * Will be used by the configuration activating the plain JPA based repository configuration 
 * (see {@link PlainJpaConfig}) as well as the Spring Data JPA based one (see {@link ApplicationConfig}).
 * 
 * Another way for specifying the location of a properties file:
 * 		@PropertySource(value = "classpath:dbconn.properties")
 * 
 * @author Alejandro Aguilera Vega
 *
 */
@Configuration
@EnableTransactionManagement
@PropertySource("file:${PROPERTIES_FILES_PATH}/dbconn.properties")
@ComponentScan("com.mfm.registry.jpa.legacy.onlinebanking")
public class InfrastructureConfig {

	private static final Logger LOG = 
			LoggerFactory.getLogger(InfrastructureConfig.class);
	
	/**
	 * 
	 */
	@Autowired 
	private Environment env;

	/**
	 * 
	 * @return
	 */
	public DriverManagerDataSource dataSource() {

		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("mur.jdbc.driverClassName"));
		dataSource.setUrl(env.getProperty("mur.jdbc.url"));
		dataSource.setUsername(env.getProperty("mur.jdbc.user"));
		dataSource.setPassword(env.getProperty("mur.jdbc.pass"));

		if (LOG.isDebugEnabled()) {
			LOG.debug("Connection info [driverClassName]: "+env.getProperty("mur.jdbc.driverClassName"));
			LOG.debug("Connection info [url]: "+env.getProperty("mur.jdbc.url"));
			LOG.debug("Connection info [user]: "+env.getProperty("mur.jdbc.user"));
		}
		return dataSource;
	}

	/*
	@Bean
	public DataSource dataSource() throws NamingException {
		Context context = new InitialContext();
		return (DataSource)context.lookup(env.getProperty("jndi.name"));
	}
	*/

	/**
	 * Sets up a {@link LocalContainerEntityManagerFactoryBean} to use Hibernate. Activates picking up entities from the
	 * project's base package.
	 * 
	 * @return
	 * @throws NamingException 
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.MYSQL);
		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(dataSource());
		factory.setPackagesToScan(getClass().getPackage().getName());
		factory.setJpaVendorAdapter(vendorAdapter);
		Map<String, Object> jpaProperties = new HashMap<String, Object>();
		jpaProperties.put("hibernate.ejb.interceptor", "com.mfm.registry.jpa.interceptor.RegistryInterceptor");
		factory.setJpaPropertyMap(jpaProperties);
		return factory;
	}

	/**
	 * 
	 * @return
	 * @throws NamingException 
	 */
	@Bean
	public PlatformTransactionManager transactionManager() throws NamingException {

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return txManager;
	}
}
