package com.mfm.registry.jpa;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.mfm.registry.jpa.core.OnlineBankingUser;

/**
 * JavaConfig class to activate component scanning to pick up
 * {@link com.oreilly.springdata.jpa.core.JpaCustomerRepository}. Re-uses the common infrastructure configuration
 * defined in {@link InfrastructureConfig}.
 * 
 * @author Alejandro Aguilera Vega
 *
 */
@Configuration
@ComponentScan(basePackageClasses = OnlineBankingUser.class)
@Import(InfrastructureConfig.class)
public class PlainJpaConfig {

}
