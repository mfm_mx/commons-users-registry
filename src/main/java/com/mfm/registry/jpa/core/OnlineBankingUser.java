package com.mfm.registry.jpa.core;

import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * Abstraction of the database table MFM_BEL_RONLINE_BANKING_USERS
 * @author Alejandro Aguilera Vega
 *
 */
@Entity
@Table(name="MFM_BEL_RONLINE_BANKING_USERS")
@Audited
public class OnlineBankingUser extends AbstractEntity {

	@Column(name="CLIENT_NUMBER",insertable=false,updatable=false)
	private String clientNumber;

	@Column(name="CLIENT_ALIAS")
	private String clientAlias;

	@Column(name="CLIENT_ID")
	private String clientId;

	@Column(name="STATUS")
	private String status;

	@Column(name="DATE_REGISTRY")
	private Timestamp dateRegistry;

	@Column(name="DATE_PWD_UPDATE")
	private Timestamp datePwdUpdate;

	@Column(name="CLIENT_FULLNAME")
	private String fullName;

	@Column(name="CLIENT_FIRST_NAME")
	private String firstName;

	@Column(name="CLIENT_MIDDLE_NAME")
	private String middleName;

	@Column(name="CLIENT_FIRST_SURNAME")
	private String firstSurName;

	@Column(name="CLIENT_SECOND_SURNAME")
	private String secondSurName;

	@Column(name="ACCOUNT_NUMBER")
	private String accountNumber;

	@Column(name="RFC")
	private String rfc;

	@Column(name="EMAIL")
	private String email;

	@Column(name="DERIVED_KEY")
	private String derivedKey;

	@Column(name="SALT")
	private String salt;

	@Column(name="INITIAL_VECTOR_KEY")
	private String initialVectorKey;

	@Column(name="INITIAL_VECTOR_SALT")
	private String initialVectorSalt;

	@Column(name="LEGACY_HASH")
	private String legacyHash;

	/** Default value 0 for later integration with HSM EFT encryption */
	@Column(name="HSM_VALIDATED")
	private int hsmValidated = 0;

	/** If needed, for logical deletion (active = 0). */
	@Column(name="ACTIVE")
	private int active = 1;

	public OnlineBankingUser(
			String clientNumber, 
			String clientAlias,
			String status,
			Timestamp dateRegistry,
			Timestamp datePwdUpdate,
			String fullName,
			String firstName,
			String middleName,
			String firstSurName,
			String secondSurName,
			String accountNumber,
			String rfc,
			String email,
			String derivedKey,
			String salt,
			String initialVectorKey,
            String initialVectorSalt,
			String legacyHash
			) {

		super(clientNumber);
		this.clientNumber=clientNumber;
		this.clientAlias = clientAlias;
		this.clientId = UUID.randomUUID().toString();
		this.status = status;
		this.dateRegistry = dateRegistry;
		this.datePwdUpdate = datePwdUpdate;
		this.fullName = fullName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.firstSurName = firstSurName;
		this.secondSurName = secondSurName;
		this.accountNumber = accountNumber;
		this.rfc = rfc;
		this.email = email;
		this.derivedKey = derivedKey;
		this.salt = salt;
		this.initialVectorKey = initialVectorKey;
        this.initialVectorSalt = initialVectorSalt;
		this.legacyHash = legacyHash;
	}

	/**
	 * 
	 * @param clientNumber
	 */
	public OnlineBankingUser(String clientNumber) {

		super(clientNumber);
		this.clientNumber=clientNumber;
	}

	/**
	 * 
	 */
	protected OnlineBankingUser() {

	}

	public String getClientNumber() {
		return this.clientNumber;
	}

	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}

	public String getClientAlias() {
		return this.clientAlias;
	}

	public void setClientAlias(String clientAlias) {
		this.clientAlias = clientAlias;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getDateRegistry() {
		return this.dateRegistry;
	}

	public void setDateRegistry(Timestamp dateRegistry) {
		this.dateRegistry = dateRegistry;
	}

	public Timestamp getDatePwdUpdate() {
		return this.datePwdUpdate;
	}

	public void setDatePwdUpdate(Timestamp datePwdUpdate) {
		this.datePwdUpdate = datePwdUpdate;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getFirstSurName() {
		return this.firstSurName;
	}

	public void setFirstSurName(String firstSurName) {
		this.firstSurName = firstSurName;
	}

	public String getSecondSurName() {
		return this.secondSurName;
	}

	public void setSecondSurName(String secondSurName) {
		this.secondSurName = secondSurName;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getRfc() {
		return this.rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDerivedKey() {
		return this.derivedKey;
	}

	public void setDerivedKey(String derivedKey) {
		this.derivedKey = derivedKey;
	}

	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getInitialVectorKey() {
		return this.initialVectorKey;
	}

	public void setInitialVectorKey(String initialVectorKey) {
		this.initialVectorKey = initialVectorKey;
	}

    public String getInitialVectorSalt() {
        return this.initialVectorSalt;
    }

    public void setInitialVectorSalt(String initialVectorSalt) {
        this.initialVectorSalt = initialVectorSalt;
    }

	public String getLegacyHash() {
		return this.legacyHash;
	}

	public void setLegacyHash(String legacyHash) {
		this.legacyHash = legacyHash;
	}

	public int getHsmValidated() {
		return this.hsmValidated;
	}

	public void setHsmValidated(int hsmValidated) {
		this.hsmValidated = hsmValidated;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}
}