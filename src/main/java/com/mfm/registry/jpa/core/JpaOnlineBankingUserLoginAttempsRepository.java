package com.mfm.registry.jpa.core;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

/**
 * Implementation of database operations that are applied to the 
 * table MFM_BEL_RONLINE_BANKING_USERS_LOGIN_ATTEMPTS
 * 
 * @author Alejandro Aguilera Vega
 *
 */
@Repository
class JpaOnlineBankingUserLoginAttemptsRepository implements OnlineBankingUserLoginAttemptsRepository {

	/**
	 * Database operations are handled with this
	 */
	@PersistenceContext
	private EntityManager em;

	/**
	 * Search for a registry using a client number (id) as filter
	 * @param id
	 */
	public OnlineBankingUserLoginAttempts findOne(String id) {
		return em.find(OnlineBankingUserLoginAttempts.class, id);
	}

	/**
	 * Insert of a new registry or Update of an existing one
	 * @param user
	 */
	public OnlineBankingUserLoginAttempts save(OnlineBankingUserLoginAttempts user) {

		if (user.getId() == null) {
			em.persist(user);
			return user;
		} else {
			return em.merge(user);
		}
	}

	/**
	 * Delete a registry
	 * @param user
	 */
	public void delete(OnlineBankingUserLoginAttempts user) {

		em.remove(user);
	}

	/**
	 * Search for a registry using a client number as filter
	 * @param clientNumber
	 * @return
	 */
	public OnlineBankingUserLoginAttempts findByClientNumber(String clientNumber) {

		TypedQuery<OnlineBankingUserLoginAttempts> query = em.createQuery(
				"select c from OnlineBankingUserLoginAttempts c where c.clientNumber = :clientNumber and c.active = 1", 
				OnlineBankingUserLoginAttempts.class);
		query.setParameter("clientNumber", clientNumber);
		return query.getSingleResult();
	}
}