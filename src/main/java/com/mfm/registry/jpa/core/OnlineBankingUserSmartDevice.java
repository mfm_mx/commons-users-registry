package com.mfm.registry.jpa.core;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 * Abstraction of the database table MFM_BEL_RONLINE_BANKING_USERS_SMARTDEVICES
 * @author Alejandro Aguilera Vega
 *
 */
@Entity
@Table(name="MFM_BEL_RONLINE_BANKING_USERS_SMARTDEVICES")
@Audited
public class OnlineBankingUserSmartDevice extends AbstractEntity {

	@Column(name="CLIENT_NUMBER",insertable=false,updatable=false)
	private String clientNumber;

	@Column(name="MANUFACTURER",nullable=false)
	private String manufacturer;

	@Column(name="BRAND",nullable=false)
	private String brand;

	@Column(name="MODEL",nullable=true)
	private String model;

	@Column(name="OS",nullable=false)
	private String os;

	@Column(name="OS_VERSION",nullable=false)
	private String osVersion;

	@Column(name="JAILBRAKE_ENABLED",nullable=true)
	private int jailbrakeEnabled;

	@Column(name="TOKEN_NAME",nullable=false)
	private String tokenName;

	@Column(name="DATE_ACTIVATION",nullable=true)
	private Timestamp dateActivation;

	@Column(name="DATE_EXPIRATION",nullable=true)
	private Timestamp dateExpiration;

	/**
	 * If needed, for logical deletion (active = 0).
	 */
	@Column(name="ACTIVE")
	private int active = 1;

	public OnlineBankingUserSmartDevice(
			String clientNumber,
			String manufacturer,
			String brand,
			String os,
			String osVersion,
			String tokenName) {
		
		super(clientNumber);
		this.clientNumber = clientNumber;
		this.manufacturer = manufacturer;
		this.brand = brand;
		this.os = os;
		this.osVersion = osVersion;
		this.tokenName = tokenName;
	}

	public OnlineBankingUserSmartDevice(
			String clientNumber,
			String manufacturer,
			String brand,
			String model,
			String os,
			String osVersion,
			String tokenName,
			int jailBrakeEnabled,
			Timestamp dateActivation,
			Timestamp dateExpiration) {
		
		super(clientNumber);
		this.clientNumber = clientNumber;
		this.manufacturer = manufacturer;
		this.brand = brand;
		this.model = model;
		this.os = os;
		this.osVersion = osVersion;
		this.tokenName = tokenName;
		this.jailbrakeEnabled = jailBrakeEnabled;
		this.dateActivation = dateActivation;
		this.dateExpiration = dateExpiration;
	}
	
	/**
	 * 
	 */
	protected OnlineBankingUserSmartDevice() {

	}

	public String getClientNumber() {
		return this.clientNumber;
	}

	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOs() {
		return this.os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getOsVersion() {
		return this.osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getTokenName() {
		return this.tokenName;
	}

	public void setTokenName(String tokenName) {
		this.tokenName = tokenName;
	}
	
	public int getJailbrakeEnabled() {
		return this.jailbrakeEnabled;
	}

	public void setJailbrakeEnabled(int jailbrakeEnabled) {
		this.jailbrakeEnabled = jailbrakeEnabled;
	}

	public Timestamp getDateActivation() {
		return this.dateActivation;
	}

	public void setDateActivation(Timestamp dateActivation) {
		this.dateActivation = dateActivation;
	}

	public Timestamp getDateExpiration() {
		return this.dateExpiration;
	}

	public void setDateExpiration(Timestamp dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}
}
