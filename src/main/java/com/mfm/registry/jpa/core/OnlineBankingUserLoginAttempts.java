package com.mfm.registry.jpa.core;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Abstraction of the database table MFM_BEL_RONLINE_BANKING_USERS_LOGIN_ATTEMPTS
 * @author Alejandro Aguilera Vega
 *
 */
@Entity
@Table(name="MFM_BEL_RONLINE_BANKING_USERS_LOGIN_ATTEMPTS")
public class OnlineBankingUserLoginAttempts extends AbstractEntity {

	@Column(name="CLIENT_NUMBER",insertable=false,updatable=false)
	private String clientNumber;

	@Column(name="LOGIN_ATTEMPTS")
	private int loginAttemps;

	@Column(name="DATE_LAST_LOGIN")
	private Timestamp dateLastLogin;

	@Column(name="DATE_LAST_ATTEMPT")
	private Timestamp dateLastAttempt;

	/**
	 * If needed, for logical deletion (active = 0).
	 */
	@Column(name="ACTIVE")
	private int active = 1;

	public OnlineBankingUserLoginAttempts(
			String clientNumber,
			int loginAttemps,
			Timestamp dateLastLogin,
			Timestamp dateLastAttempt) {
		
		super(clientNumber);
		this.clientNumber = clientNumber;
		this.loginAttemps = loginAttemps;
		this.dateLastLogin = dateLastLogin;
		this.dateLastAttempt = dateLastAttempt;
	}

	/**
	 * 
	 * @param clientNumber
	 */
	public OnlineBankingUserLoginAttempts(String clientNumber) {

		super(clientNumber);
		this.clientNumber=clientNumber;
	}

	/**
	 * 
	 */
	protected OnlineBankingUserLoginAttempts() {

	}

	public String getClientNumber() {
		return this.clientNumber;
	}

	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}

	public int getLoginAttemps() {
		return this.loginAttemps;
	}

	public void setLoginAttemps(int loginAttemps) {
		this.loginAttemps = loginAttemps;
	}

	public Timestamp getDateLastLogin() {
		return this.dateLastLogin;
	}

	public void setDateLastLogin(Timestamp dateLastLogin) {
		this.dateLastLogin = dateLastLogin;
	}

	public Timestamp getDateLastAttempt() {
		return this.dateLastAttempt;
	}

	public void setDateLastAttempt(Timestamp dateLastAttempt) {
		this.dateLastAttempt = dateLastAttempt;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}
}
