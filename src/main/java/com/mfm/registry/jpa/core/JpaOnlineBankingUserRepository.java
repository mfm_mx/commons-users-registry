package com.mfm.registry.jpa.core;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

/**
 * Implementation of database operations that are applied to the 
 * table MFM_BEL_RONLINE_BANKING_USERS
 * 
 * @author Alejandro Aguilera Vega
 * 
 */
@Repository
class JpaOnlineBankingUserRepository implements OnlineBankingUserRepository {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Search for a registry using a client number (id) as filter
	 * @param id
	 */
	public OnlineBankingUser findOne(String id) {
		return em.find(OnlineBankingUser.class, id);
	}

	/**
	 * Insert of a new registry or Update of an existing one
	 * @param user
	 */
	public OnlineBankingUser save(OnlineBankingUser user) {

		if (user.getId() == null) {
			em.persist(user);
			return user;
		} else {
			return em.merge(user);
		}
	}

	/**
	 * Delete a registry
	 * @param user
	 */
	public void delete(OnlineBankingUser user) {

		em.remove(user);
	}

	/**
	 * Search for a registry using a client number as filter
	 * @param clientNumber
	 * @return
	 */
	public OnlineBankingUser findByClientNumber(String clientNumber) {

		TypedQuery<OnlineBankingUser> query = em.createQuery(
				"select c from OnlineBankingUser c where c.clientNumber = :clientNumber and c.active = 1", 
				OnlineBankingUser.class);
		query.setParameter("clientNumber", clientNumber);
		return query.getSingleResult();
	}

	/**
	 * Search for a registry using a client alias as filter
	 * @param clientAlias
	 * @return
	 */
	public OnlineBankingUser findByClientAlias(String clientAlias) {

		TypedQuery<OnlineBankingUser> query = em.createQuery(
				"select c from OnlineBankingUser c where c.clientAlias = :clientAlias and c.active = 1", 
				OnlineBankingUser.class);
		query.setParameter("clientAlias", clientAlias);
		return query.getSingleResult();
	}
}