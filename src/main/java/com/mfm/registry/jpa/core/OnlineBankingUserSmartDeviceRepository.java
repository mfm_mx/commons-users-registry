package com.mfm.registry.jpa.core;

import org.springframework.data.repository.Repository;

/**
 * 
 * @author Alejandro Aguilera Vega
 * 
 */
public interface OnlineBankingUserSmartDeviceRepository extends Repository<OnlineBankingUserSmartDevice, String> {

	OnlineBankingUserSmartDevice findOne(String id);
	OnlineBankingUserSmartDevice save(OnlineBankingUserSmartDevice userSmartDevice);
	OnlineBankingUserSmartDevice findByClientNumber(String clientNumber);
}
