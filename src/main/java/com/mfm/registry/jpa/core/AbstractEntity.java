package com.mfm.registry.jpa.core;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base class to derive entity classes from.
 * For future implementations, We could add the columns:
 * 
 * @Column(name = "created_date")
 * @CreatedDate
 * private long createdDate;
 *
 * @Column(name = "modified_date")
 * @LastModifiedDate
 * private long modifiedDate;
 * 
 * @author Alejandro Aguilera Vega
 *
 */
@MappedSuperclass
public class AbstractEntity {

	/**
	 * Unique Identifier for an Online Banking User
	 */
	@Id
	@Column(name="CLIENT_NUMBER")
	private String id;

	public AbstractEntity(){
	}

	/**
	 * This constructor should be used only if a new registry is going
	 * to be inserted in the database
	 * @param id (clientNumber)
	 */
	public AbstractEntity(String id){
		this.id = id;
	}

	/**
	 * Returns the clientNumber
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Generic implementation for comparing with other objects of this type
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (this.id == null || obj == null || !(this.getClass().equals(obj.getClass()))) {
			return false;
		}

		AbstractEntity that = (AbstractEntity) obj;

		return this.id.equals(that.getId());
	}

	/**
	 * Returns a calculated HashCode
	 */
	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}
}
