package com.mfm.registry.jpa.core;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

/**
 * Implementation of database operations that are applied to the 
 * table MFM_BEL_RONLINE_BANKING_USERS_SMARTDEVICES
 * 
 * @author Alejandro Aguilera Vega
 *
 */
@Repository
class JpaOnlineBankingUserSmartDeviceRepository implements OnlineBankingUserSmartDeviceRepository {

	/**
	 * Database operations are handled with this
	 */
	@PersistenceContext
	private EntityManager em;

	/**
	 * Search for a registry using a client number (id) as filter
	 * @param id
	 */
	@Override
	public OnlineBankingUserSmartDevice findOne(String id) {
		return em.find(OnlineBankingUserSmartDevice.class, id);
	}

	/**
	 * Insert of a new registry or Update of an existing one
	 * @param userSmartDevice
	 */
	@Override
	public OnlineBankingUserSmartDevice save(OnlineBankingUserSmartDevice userSmartDevice) {

		if (userSmartDevice.getId() == null) {
			em.persist(userSmartDevice);
			return userSmartDevice;
		} else {
			return em.merge(userSmartDevice);
		}
	}

	/**
	 * Search for a registry using a client number as filter
	 * @param clientNumber
	 * @return
	 */
	@Override
	public OnlineBankingUserSmartDevice findByClientNumber(String clientNumber) {

		TypedQuery<OnlineBankingUserSmartDevice> query = em.createQuery(
				"select c from OnlineBankingUserSmartDevice c where c.clientNumber = :clientNumber and c.active = 1", 
				OnlineBankingUserSmartDevice.class);
		query.setParameter("clientNumber", clientNumber);
		return query.getSingleResult();
	}
}
