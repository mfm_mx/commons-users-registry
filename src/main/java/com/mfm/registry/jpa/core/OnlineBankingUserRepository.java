package com.mfm.registry.jpa.core;

import org.springframework.data.repository.Repository;

/**
 * 
 * @author Alejandro Aguilera Vega
 * 
 */
public interface OnlineBankingUserRepository extends Repository<OnlineBankingUser, String> {

	OnlineBankingUser findOne(String id);
	OnlineBankingUser save(OnlineBankingUser user);
	void delete(OnlineBankingUser user);
	OnlineBankingUser findByClientNumber(String clientNumber);
	OnlineBankingUser findByClientAlias(String clientAlias);
}
