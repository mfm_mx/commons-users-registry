package com.mfm.registry.jpa.core;

import org.springframework.data.repository.Repository;

/**
 * 
 * @author Alejandro Aguilera Vega
 * 
 */
public interface OnlineBankingUserLoginAttemptsRepository extends Repository<OnlineBankingUserLoginAttempts, String> {

	OnlineBankingUserLoginAttempts findOne(String id);
	OnlineBankingUserLoginAttempts save(OnlineBankingUserLoginAttempts user);
	OnlineBankingUserLoginAttempts findByClientNumber(String clientNumber);
}
