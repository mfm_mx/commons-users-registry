# commons-users-registry
---

## Index

* [**Description.**](#description)
* [**Pre-requisites.**](#preRequisites)
* [**Configuration.**](#configuration)
* [**Packaging.**](#packaging)
* [**Deployment.**](#deployment)
* [**List of Services.**](#listOfServices)
* [**Changelog.**](#changelog)
* [**Additional Resources.**](#additionalResources)

## Description
JPA Persistent Unit component responsible of managing MFM's Online Users information on Galera Cluster.

[Back to Index ^](#index)

## Pre-requisites
For this component to work properly, some pre-requisites are needed:
* Java 8.
* Connection to a MySQL database.

[Back to Index ^](#index)

## Configuration
The following configuration must be present:
* An environment variable named `PROPERTIES_FILES_PATH`, with a value corresponding to the path where the `dbconn.properties` file is located.
* The `dbconn.properties` file must contain the following information (QA configuration as sample):

```
mur.jndi.name=jdbc/master_users_registry
mur.jdbc.driverClassName=com.mysql.jdbc.Driver
mur.jdbc.user=oauth_mur
mur.jdbc.url=jdbc:mysql://[HOSTNAME]:3306/master_users_registry
mur.jdbc.pass=password
```

[Back to Index ^](#index)

## Packaging
In order to compile and package this component in it's JAR form, [maven 3.0](https://maven.apache.org/) or above is needed, just type the command `mvn clean package` where the `pom.xml` file is, and the `commons-users-registry-x.x.x.jar` file will be created at `commons-users-registry/target`. This procedure assumes that a NEXUS Repository User is registered, and the proper configuration at the `.m2` folder exists.

[Back to Index ^](#index)

## Deployment
This component is deployed as a dependency for other projects.

[Back to Index ^](#index)

## List of Services
No services exposed as APIs.

[Back to Index ^](#index)

## Changelog
| VERSION       | DESCRIPTION  |
|:-------------:|:-------------|
| 1.1.0         | Support for component commons-crypto which had a bug, fixed on its version 1.1.0 |
| 1.0.0         | First version of the component. |

[Back to Index ^](#index)

## Additional Resources

* [Java Persistence API](http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html)

[Back to Index ^](#index)
